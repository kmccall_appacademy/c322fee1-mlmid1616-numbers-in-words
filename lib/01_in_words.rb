class Fixnum



  def in_words
    return "zero" if self == 0
    return "one" if self == 1
    return "two" if self == 2
    return "three" if self == 3
    return "four" if self == 4
    return "five" if self == 5
    return "six" if self == 6
    return "seven" if self == 7
    return "eight" if self == 8
    return "nine" if self == 9

    result = []
    times_to_run = (self.to_s.length.to_f / 3).ceil
    if times_to_run == 1
      result << three_spots(self)
    end

    if times_to_run == 2
      result << three_spots(self / 1000)
      result << "thousand"
      result << three_spots(self % 1000)
    end

    if times_to_run == 3
      result << three_spots(self / 1000000)
      result << "million"
      result << three_spots(self % 1000000 / 1000)
      result << "thousand" if (self % 1000000 / 1000) > 0
      result << three_spots(self % 1000)
    end

    if times_to_run == 4
      result << three_spots(self / 1000000000)
      result << "billion"
      result << three_spots(self % 1000000000 / 1000000)
      result << "million" if (self % 1000000000 / 1000000) > 0
      result << three_spots(self % 1000000 / 1000 )
      result << "thousand" if (self % 1000000 / 1000) > 0
      result << three_spots(self % 1000)
    end

    if times_to_run == 5
      result << three_spots(self / 1000000000000)
      result << "trillion"
      result << three_spots(self % 1000000000000 / 1000000000)
      result << "billion" if (self % 1000000000000 / 1000000000) > 0
      result << three_spots(self % 1000000000 / 1000000)
      result << "million" if (self % 1000000000 / 1000000) > 0
      result << three_spots(self % 1000000 / 1000)
      result << "thousand" if (self % 1000000 / 1000) > 0
      result << three_spots(self % 1000)
    end

    result.flatten.join(" ")
  end



private
  def three_spots(num)

  letters_single = ["zero","one","two","three","four","five","six","seven","eight","nine"]
  letters_double = ["twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]
  letters_weird = ["ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]
  numbered_single = (0..9).to_a
  numbered_double = (2..9).to_a
  weird_numbered = (10..19).to_a

    result = []
    continue = true
    if num / 100 > 0
      if num % 100 > 0
        result << letters_single[numbered_single.index(num/100)]
        result << "hundred"
      else
        result << letters_single[numbered_single.index(num/100)]
        result << "hundred"
      end
    end

    if weird_numbered.include?(num % 100)
      result << letters_weird[weird_numbered.index(num % 100)]
      continue = false
    end

    if continue && (num % 100 / 10 > 0)
      if num % 10 > 0
        result << letters_double[numbered_double.index(num % 100 / 10)]
      else
        result << letters_double[numbered_double.index(num % 100 / 10)]
      end
    end

    if continue && (num % 10 > 0 )
      result << letters_single[numbered_single.index(num % 10)]
    end

    result
  end

end
